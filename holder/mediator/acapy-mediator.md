# aries-cloud-agent-python mediator for Holder role

ACApy (Aries Cloud Agent Python) is an implementation of the Hyperledger Aries project able to communicate with the Hyperledger Indy ledger. In the Mediator mode, this service routes requests to agents without public static endpoints, e.g. Mobile Wallet of a Holder.

[ACApy repository](https://github.com/hyperledger/aries-cloudagent-python)

The agent exposes either HTTP endpoint or WebSocket (WS) endpoint. The agent is accompanied by a _Tails_ server, which records revocation of credentials. Last component of the stack is a database used by the agent to store its data.

## Requirements

* machine with IP / domain name
* installation of Docker engine on the machine
* web server to act as reverse-proxy and forward requests to docker containers
* Docker-compose to run the container

## Running

* Build Docker container of the aca-py
* Create docker-compose
* Spin up the instance with `docker-compose up -d`
* Expose interfaces into the world (e.g. using Apache webserver reverse-proxy)

### Building Docker container - ACA-py

* Clone acapy repository
* enter clonned directory
* build the image with Docker `docker build -f docker/Dockerfile -t aries-acapy .`
  * to allow `indy` wallet type, build the _indy_ version of Dockerfile: `docker build -f docker/Dockerfile.indy -t aries-acapy .`


### Docker-compose file

```
---

version: '3'

networks:
  mediator-network:

services:
  acapy:
    image: acapy:latest
    container_name: mediator-acapy
    depends_on:
      - db
    restart: unless-stopped
    ports:
      - "${MEDIATOR_AGENT_HTTP_IN_PORT_HOST}:${MEDIATOR_AGENT_HTTP_IN_PORT}"
      - "${MEDIATOR_AGENT_WS_IN_PORT_HOST}:${MEDIATOR_AGENT_WS_IN_PORT}"
      - "${MEDIATOR_AGENT_HTTP_ADMIN_PORT_HOST}:${MEDIATOR_AGENT_HTTP_ADMIN_PORT}"
    environment:
      WAIT_HOSTS: "mediator-db:5432"
      WAIT_HOSTS_TIMEOUT: "300"
      WAIT_SLEEP_INTERVAL: "5"
      WAIT_HOST_CONNECT_TIMEOUT: "3"
    entrypoint: /bin/bash
    command: [
       "-c",
      "aca-py start \
        --label '${MEDIATOR_AGENT_LABEL}' \
        --auto-provision \
        --auto-ping-connection \
        --auto-accept-requests \
        --auto-accept-invites \
        --emit-new-didcomm-prefix \
        --connections-invite \
        --invite-label '${MEDIATOR_AGENT_LABEL}' \
        --invite-multi-use \
        --open-mediation \
        --no-ledger \
        --public-invites \
        --enable-undelivered-queue \
        --inbound-transport 'http' '0.0.0.0' '${MEDIATOR_AGENT_HTTP_IN_PORT}' \
        --inbound-transport 'ws' '0.0.0.0' '${MEDIATOR_AGENT_WS_IN_PORT}' \
        --outbound-transport 'http' \
        --outbound-transport 'ws' \
        --endpoint '${MEDIATOR_AGENT_HTTP_ENDPOINT_URL}' '${MEDIATOR_AGENT_WS_ENDPOINT_URL}' \
        --seed '${MEDIATOR_AGENT_WALLET_SEED}' \
        --wallet-type '${MEDIATOR_AGENT_WALLET_TYPE}' \
        --wallet-name '${MEDIATOR_AGENT_WALLET_NAME}' \
        --wallet-key '${MEDIATOR_AGENT_WALLET_KEY}' \
        --wallet-storage-type postgres_storage \
        --wallet-storage-config '{\"url\":\"mediator-db:5432\",\"max_connections\":5}'
        --wallet-storage-creds '{\"account\":\"${MEDIATOR_DATABASE_USERNAME}\",\"password\":\"${MEDIATOR_DATABASE_PASSWORD}\",\"admin_account\":\"${MEDIATOR_DATABASE_USERNAME}\",\"admin_password\":\"${MEDIATOR_DATABASE_PASSWORD}\"}'
        --admin '0.0.0.0' '${MEDIATOR_AGENT_HTTP_ADMIN_PORT}' \
        --admin-insecure-mode \
        --trace \
        --trace-target 'log' \
        --trace-tag 'acapy.mediator' \
        --trace-label 'acapy.mediator' \
        --log-level 'debug'"
    ]
    networks:
      - mediator-network
  db:
    image: postgres:latest
    container_name: mediator-db
    hostname: mediator-db
    networks:
      - mediator-network
    environment:
      POSTGRES_USER: ${MEDIATOR_DATABASE_USERNAME}
      POSTGRES_PASSWORD: ${MEDIATOR_DATABASE_PASSWORD}
      POSTGRES_DB: ${MEDIATOR_DATABASE_DB}
    volumes:
      - ./.postgres:/var/lib/postgresql
```

## Configuration

The docker-compose file expects some variables to be either hardcoded or passed via the .env file. For the example docker-compose file above, we have used .env file like:

```
MEDIATOR_DATABASE_USERNAME=postgres
MEDIATOR_DATABASE_PASSWORD=[PASS]
MEDIATOR_DATABASE_DB=ssi_mediator

MEDIATOR_AGENT_HTTP_IN_PORT=9180
MEDIATOR_AGENT_HTTP_IN_PORT_HOST=9180
MEDIATOR_AGENT_WS_IN_PORT=9182
MEDIATOR_AGENT_WS_IN_PORT_HOST=9182
MEDIATOR_AGENT_HTTP_ADMIN_PORT=9184
MEDIATOR_AGENT_HTTP_ADMIN_PORT_HOST=9184
MEDIATOR_AGENT_DB_PORT=5342
MEDIATOR_AGENT_WEBHOOK_URL=
MEDIATOR_AGENT_LABEL=FR CESNET Mediator
MEDIATOR_AGENT_HTTP_ENDPOINT_URL=https://host:9181
MEDIATOR_AGENT_WS_ENDPOINT_URL=wss://host:9183
MEDIATOR_AGENT_WALLET_SEED=[SEED]
MEDIATOR_AGENT_WALLET_TYPE=askar
MEDIATOR_AGENT_WALLET_NAME=mediator_wallet
MEDIATOR_AGENT_WALLET_KEY=[KEY]
MEDIATOR_AGENT_WALLET_STORAGE_TYPE=postgres_storage
```

### Example reverse proxy in Apache

The stack exposes several endpoints:

* HTTP endpoint for agent API
* WS endpoint for agent API
* Swagger UI to control agent (admin ui)

We have been using Apache webserver to expose these endpoints with SSL enabled and forward the traffic into corresponding container.


Example configuration of VirtualHost:

```
# MEDIATOR - HTTP
<VirtualHost *:9181>
        ServerName mediator.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/mediator-http-error.log
        CustomLog ${APACHE_LOG_DIR}/mediator-http-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / http://0.0.0.0:9180/
        ProxyPassReverse / http://0.0.0.0:9180/
</VirtualHost>

# MEDIATOR - WS
<VirtualHost *:9183>
        ServerName mediator.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/mediator-ws-error.log
        CustomLog ${APACHE_LOG_DIR}/mediator-ws-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / ws://0.0.0.0:9182/
        ProxyPassReverse / ws://0.0.0.0:9182/
</VirtualHost>

# MEDIATOR - ADMIN
<VirtualHost *:9185>
        ServerName mediator.server.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/mediator-admin-error.log
        CustomLog ${APACHE_LOG_DIR}/mediator-admin-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

        ProxyPass        / http://0.0.0.0:9184/
        ProxyPassReverse / http://0.0.0.0:9184/
</VirtualHost>
```

