# Indy network

Indy network is the implementation of a DLT under the [Hyperledger Indy project](https://www.hyperledger.org/use/hyperledger-indy).

We have chosen to deploy BCGovs' _von network_ project. Original project can be found at [GitHub von-network repository](https://github.com/bcgov/von-network). The used version has been the commit `4dfeeb12d683ae2bb2075e2537d91797a21d6b28` (at the time the last commit in the `main` branch).

## Requirements

* machine with IP / domain name
* installation of Docker engine on the machine
* web server to act as reverse-proxy and forward requests to docker containers

## Configuration

No static configuration changes have been made.

## Running

* Clone the repository
* Enter the clonned repository
* Build containers with command `./manage build`
  * TIP: if you run into issues with bulding containers, modify the `manage` script to switch to `network=host`. Example change: `docker build --network=host $(initDockerBuildArgs) -t von-network-base .`
* Bootstrap the network with command `./manage start [domain_name | IP] WEB_SERVER_HOST_PORT=[PORT] "LEDGER_INSTANCE_NAME=[LEDGER NAME]" &`
  * The `domain_name` or `IP` are used to contact Indy nodes. Strongly advised to have a publicly resolvable domain name or IP
  * `PORT` - configure port on which the management UI will be exposed. We have used the port as reverse-proxy port in Apache webserver.
  * `LEDGER NAME` - human readable string that will be displayed in UIs as the name of the ledger.
* Check the logs with command `./manage logs -f`
* When you see all containers are fine and Nodes have been able to interconnect, you can call `disown` to keep job running after you sign out of the machine
* To stop running containers, just call `./manage down`.

### Example reverse proxy in Apache

Modify the config of your webserver to expose the WEB UI into the world. Example configuration of VirtualHost:

```
<VirtualHost *:443>
        ServerName ledger.public.net
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/network-error.log
        CustomLog ${APACHE_LOG_DIR}/network-access.log combined

        SSLCertificateFile chain.pem
        SSLCertificateKeyFile privkey.pem

	# LEDGER HAS BEEN RUN WITH FLAG WEB_SERVER_HOST_PORT=9780
        ProxyPass        / http://0.0.0.0:9780/
        ProxyPassReverse / http://0.0.0.0:9780/
</VirtualHost>
```

